export default function <T>() {
  const pages: { options: T, $route: { query: T } }[] = getCurrentPages()
  const page = pages[pages.length - 1]
  console.log('pages', pages)
  let query
  // #ifdef H5
    query = page.$route.query
  // #endif
  // #ifndef H5
    query = page.options
  // #endif
  return query
}

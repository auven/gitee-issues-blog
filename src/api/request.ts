import Request, { config, requestConfig, response } from '../utils/luch-request_1.0.6/luch-request-ts/request'

const instance = new Request()
instance.setConfig = (config: config) => { /* 设置全局配置 */
  // config.baseUrl = '/dev-api'
  config.baseUrl = 'https://gitee.com/api/v5'
  return config
}
instance.interceptor.request((config: requestConfig, cancel: Function) => { /* 请求之前拦截器 */
  config.header = {
    ...config.header,
    a: 1
  }
  /*
    if (!token) { // 如果token不存在，调用cancel 会取消本次请求，但是该函数的catch() 仍会执行
        cancel('token 不存在') // 接收一个参数，会传给catch((err) => {}) err.errMsg === 'token 不存在'
    }
    */
  return config
})
instance.interceptor.response((response: response) => { /* 请求之后拦截器 */
  return response
}, (response: response) => { /* 对响应错误做点什么 */
  return response
})

export default instance

import request from './request'
import config from '../config'

export function getIssuesList(params: { labels?: string, page: number, per_page: number } = { page: 1, per_page: 10 }) {
  return request.get(`/repos/${config.owner}/${config.repo}/issues`, {
    params: {
      creator: config.owner,
      ...params
    }
  })
}

// 获取 label
export function getLabelList() {
  return request.get(`/repos/${config.owner}/${config.repo}/labels`)
}

// 获取某个 issues
export function getIssueDetail(number: string) {
  return request.get(`/repos/${config.owner}/${config.repo}/issues/${number}`)
}

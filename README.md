# gitee-issues-blog

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## 解决跨域

manifest.json
```
{
  ...
  "h5": {
    "devServer": {
      "port": 8000,
      "disableHostCheck": true,
      "proxy": {
        "/dev-api": {
          "target": "https://api.github.com/repos",
          "changeOrigin": true,
          "secure": false,
          "pathRewrite": {
            "^/dev-api": ""
          }
        }
      }
    }
  }
}
```

## TODO

修改富文本解析样式，保证多平台下渲染一致。